/*
 * Copyright (c) 2016-2018, Leftshift One
 * __________________
 * [2018] Leftshift One
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Leftshift One and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Leftshift One
 * and its suppliers and may be covered by Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Leftshift One.
 */

package gaia.client.util;

import org.json.JSONObject;

import java.util.Map;

/**
 * Utility class for JSON functionality.
 */
public final class JSON {

    private JSON() {
        super();
    }

    public static String write(Map<String, Object> map) {
        JSONObject json = new JSONObject(map);
        return json.toString(4);
    }

}
