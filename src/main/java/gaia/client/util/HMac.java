/*
 * Copyright (c) 2016-2018, Leftshift One
 * __________________
 * [2018] Leftshift One
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Leftshift One and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Leftshift One
 * and its suppliers and may be covered by Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Leftshift One.
 */

package gaia.client.util;

import gaia.client.GaiaHttpClientException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

/**
 * Utility class for HMac functionality.
 */
public final class HMac {

    private final Mac mac;

    public HMac(String secret) {
        this.mac = initMac(secret);
    }

    public String hash(String data) {
        try {
            final byte[] bytes = mac.doFinal(data.getBytes("ASCII"));
            final Formatter formatter = new Formatter();
            for (byte b : bytes) {
                formatter.format("%02x", b);
            }
            return formatter.toString();
        } catch (UnsupportedEncodingException e) {
            throw new GaiaHttpClientException(e);
        }
    }

    private static Mac initMac(String key) {
        try {
            final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "HmacSHA512");
            final Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKeySpec);

            return mac;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new GaiaHttpClientException(e);
        }
    }

}
