package gaia.client.examples;

import gaia.client.GaiaHttpClient;

import java.util.HashMap;
import java.util.Map;

/**
 * This example demonstrates how to execute a named entity recognition (NER) statement against the G.A.I.A. system.
 */
public class NerExample {

    public static void main(String[] args) {
        final String apiKey = "<<your API key>>";
        final String apiSecret = "<<your API secret>>";

        final String statement = "query Atlas($text: String!) { nlu(text: $text) { txt ner { location datetime price duration person accomodation }}}";
        final Map<String, Object> variables = new HashMap<>();
        variables.put("text", "<<your text>>");

        final GaiaHttpClient client = new GaiaHttpClient(apiKey, apiSecret);

        final Map<String, Object> result = client.execute(statement, variables);
        System.out.println(result);
    }

}
