package gaia.client;

/**
 * Runtime exception used for G.A.I.A. http client errors.
 */
public class GaiaHttpClientException extends RuntimeException {

    public GaiaHttpClientException(Throwable cause) {
        super(cause);
    }

}
