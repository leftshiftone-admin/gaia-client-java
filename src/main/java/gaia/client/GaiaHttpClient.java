/*
 * Copyright (c) 2016-2018, Leftshift One
 * __________________
 * [2018] Leftshift One
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Leftshift One and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Leftshift One
 * and its suppliers and may be covered by Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Leftshift One.
 */

package gaia.client;

import gaia.client.util.HMac;
import gaia.client.util.Http;
import gaia.client.util.JSON;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class GaiaHttpClient {

    private static final AtomicLong nonce = new AtomicLong(new SecureRandom().nextLong());

    private final HMac hmac;
    private final Http http;
    private final String apiKey;

    public GaiaHttpClient(String apiKey, String apiSecret) {
        this.hmac = new HMac(apiSecret);
        this.http = new Http("https://api.leftshift.one/atlas/api/v1");
        this.apiKey = apiKey;
    }

    /**
     * Sends a POST request to the API endpoint.
     */
    public Map<String, Object> execute(String statement, Map<String, Object> variables) {
        final Map<String, Object> body = new HashMap<>();
        body.put("statement", statement);
        body.put("variables", variables);
        body.put("timestamp", System.currentTimeMillis());
        body.put("nonce", Long.toString(nonce.getAndIncrement()));

        final String payload = JSON.write(body);
        final String payloadHMac = hmac.hash(base64(payload));

        final JSONObject json = http.post(apiKey, payloadHMac, payload);
        return json.toMap();
    }

    private String base64(String str) {
        return java.util.Base64.getEncoder().encodeToString(str.getBytes());
    }

}